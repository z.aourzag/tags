# @z.aourzag/tags
[![NPM version](https://img.shields.io/npm/dw/@z.aourzag/tags.svg?style=flat) ](https://www.npmjs.com/package/@z.aourzag/tags) [![NPM monthly downloads](https://img.shields.io/npm/dw/@z.aourzag/tags.svg?style=flat)](https://www.npmjs.com/package/@z.aourzag/tags) [![NPM total downloads](https://img.shields.io/npm/dw/@z.aourzag/tags.svg?style=flat)](https://www.npmjs.com/package/@z.aourzag/tags)
[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/z.aourzag/tags?label=pipeline)](https://gitlab.com/z.aourzag/tags/builds)
[![coverage report](https://gitlab.com/z.aourzag/tags/badges/master/coverage.svg)](https://gitlab.com/z.aourzag/tags/-/commits/master)
Small tag parser based on [bread-tags](https://github.com/Soumil07/bread-tags).

Original licensed by Soumil07 under BSD-3.
